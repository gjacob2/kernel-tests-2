summary: Verifies that lockdown mode prevents the loading of an unsigned kernel module.
description: |
        Checks the current state of lockdown. Sets to 'integrity', if not already set.
        Ensure that Alt+SysRq+x, when sent to the console, does not alter the lockdown state.
        Ensure that lockdown mode prevents unsigned kernel module loading.
        Test Inputs:
            change_cmdline 'lockdown=integrity'
            reboot
            clone external repository, https://github.com/xairy/unlockdown.git, for Python module.
            run python module evdev-sysrq.py
            If Python module was successful, then log success and ensure lockdown status remains as 'integrity'.
            If Python module was not successful, then log failure.
            module source, hw.c
            trigger, insmod hw.ko
            capture, dmesg > dmesg-unsigned.log
            check, rlAssertGrep "Lockdown: insmod: unsigned module loading is restricted" dmesg-unsigned.log
        Expected results:
            [   PASS   ] :: Command 'change_cmdline 'lockdown=integrity'' (Expected 0, got 0)
            [   PASS   ] :: Command 'git clone https://github.com/xairy/unlockdown.git' (Expected 0, got 0)
            [   PASS   ] :: Command 'python evdev-sysrq.py' (Expected 0-255, got 255)
            Python module success log statement:
                [   LOG    ] :: Alt+SysRq+x key clicks submitted, checking lockdown status.
            Python module failure log statement
                [   LOG    ] :: Alt+SysRq+x key clicks were not submitted. Skipping lockdown status check.
            [   PASS   ] :: Command 'make all' (Expected 0, got 0)
            [   PASS   ] :: Command 'make test' (Expected 0, got 0)
            [   PASS   ] :: File 'dmesg-unsigned.log' should contain 'Loading of unsigned module is rejected'
        Results location:
            output.txt | taskout.log, log is dependent upon the test executor.
contact: Philip Daly <pdaly@redhat.com>
id: a9c1231d-5bcb-4c41-9830-8be54b75dd9d
component:
  - kernel
test: bash ./runtest.sh
framework: beakerlib
recommend:
  - abootimg
  - grubby
require:
  - make
  - python3-evdev
  - type: file
    pattern:
      - /kernel-include
      - /cmdline_helper
      - /cki_lib
duration: 15m
extra-summary: /kernel-tests/security/integrity/lockdown
extra-task: /kernel-tests/security/integrity/lockdown
