summary: Verifies attempting to violate the kernel memory address space is prevented.
description: |
    Verifies attempting to violate the kernel memory address space is prevented and results in a failure (e.g. kernel panic).
    This test attempts to access an unmapped kernel address.
    Test Inputs:
        module source, violate_kernel_memory.c
        trigger, echo 1 > /sys/kernel/vkm/write_um_crash
        capture, dmesg > dmesg-crash.log
        check, rlAssertGrep "Unable to handle kernel paging request" dmesg-crash.log
    Expected results:
        [   PASS   ] :: (SEGFAULT expected) (Expected 139, got 139)
        [   PASS   ] :: File 'dmesg-crash.log' should contain 'Unable to handle kernel paging request'
    Results location:
        output.txt
contact: Stephen Bertram <sbertram@redhat.com>
component:
  - kernel
test: bash ./runtest.sh
id: ceb72ce1-3a0c-405a-9045-c078a13cf787
framework: beakerlib
require:
  - make
  - iputils
  - type: file
    pattern:
      - /memory/mmra/violate_kernel_memory/src
      - /kernel-include
duration: 10m
