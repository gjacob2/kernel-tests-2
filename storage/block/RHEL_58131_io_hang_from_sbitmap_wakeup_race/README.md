# storage/block/RHEL_58131_io_hang_from_sbitmap_wakeup_race

Storage: IO hang from sbitmap wakeup race

## How to run it

Please refer to the top-level README.md for common dependencies.

### Install dependencies

```bash
root# bash ../../../cki_bin/pkgs_install.sh metadata
```

### Execute the test

```bash
bash ./runtest.sh
```
